Remote import - Hostmaster
==========================

This Drupal module allows for fetching sites from and deploying sites to remote
Aegir servers.

A note on the project's history
-------------------------------

This project is a direct descendant of remote_import and hosting_remote_import.
For Aegir 3, we are looking at merging the back-end Drush extensions into the
front-end Drupal modules (see: https://www.drupal.org/node/2300537). As such,
this functionality will require special care to install for the time-being, and
until such time as this change is made to Aegir core.

That said, we'll still want to benefit from bug-fixes and other improvements
made to the separate projects, that will contoinue to be supported during the
Aegir 2 lifecycle. To that end, I used a git sub-tree merge in order to bring
remote_import into hosting_remote_import, under the 'provision/' directory:

    $ git remote add remote_import http://git.drupal.org/project/remote_import.git
    $ git fetch remote_import
    warning: no common commits
    [...]
    $ git read-tree --prefix=provision/ -u remote_import/7.x-3.x 
    $ git commit -am"Add remote_import under this project."
    $ git merge -s subtree remote_import/7.x-3.x

The documentation on git sub-tree merging that I followed can be found here:

    http://git-scm.com/book/en/Git-Tools-Subtree-Merging

In this case, we want to preserve the git histories of both projects and merge
them together, so we omit the '--squash' option. Another thing to note is that
we have 7.x-3.x branches in both project, and so we have to specify which
branch we want to merge from (i.e. 'remote_import/7.x-3.x').

To pull in changes to this 'ancestor' project, we should be able to run:

    $ git fetch remote_import
    $ git merge -s subtree remote_import/7.x-3.x


Installation
------------

Install this module like any other, and enable in the usual way.

You also need to install the backend portion of this extension for this to work
correctly. As discussed above, with Aegir 3 this will no longer mean deploying
a separate Drush extension. Instead, we can include the 'provision/' directory
from this project whenever the 'aegir' user runs Drush. We accomplish this by
adding something like the following to /var/aegir/.drush/drushrc.php:

$options['include'] = array (
  'hosting_remote' => '/var/aegir/hostmaster-7.x-3.x/sites/push.local/modules/hosting_remote',
);

Once https://www.drupal.org/node/2300537 lands in Aegir core, this will no
longer be required, as Aegir will register enabled Hosting features automa-
tically.


Usage
-----

You'll need to add the remote Aegir server as any other server in the frontend,
selecting ONLY the 'hostmaster' remote service as you do so.

This means, of course, that you'll need to add your ssh key to this server and
set it up like other remote servers. Note that you don't need to install
anything other than the SSH key on this server.

A general guide to setting up SSH on remote servers can be found here:
http://community.aegirproject.org/node/30#SSH_keys

Once you've set up your server in the frontend you should get a new menu item
called: 'Import remote sites' when viewing the server node. Click on that link
and follow the instructions there.
